import json,httpx,time

with open('ressources/secret.json', 'r') as f:
  credentials = json.load(f)
with open('ressources/config.json', 'r') as f:
  config = json.load(f)
with open('ressources/counts.json', 'r') as f:
  count = json.load(f)

from twitchio.ext import commands

savedState=[]

def handle_wled_return(wled_ans):
    if(wled_ans.is_success==True):
        return True
    else:
        return False
def setColors(input_colors):
    dataJSON={"seg":[{"col":[]}]}
    for i in range(0,3):
        if i < len(input_colors):
            dataJSON["seg"][0]["col"].append([input_colors[i]["R"],input_colors[i]["G"],input_colors[i]["B"],0])
        else:
            dataJSON["seg"][0]["col"].append([0,0,0,0])
    JSONized=json.dumps(dataJSON)
    r = httpx.post(config["url"], data=JSONized)
    print(r.text)
    return handle_wled_return(r)

def updateCountsFile(count_data):
    # Serializing json
    JSONized_count = json.dumps(count_data, indent=4)

    # Writing to counts.json
    with open('ressources/counts.json', "w") as outfile:
        outfile.write(JSONized_count)

async def getColorsFromParams(ctx):
    tmp = ctx.message.content.split(f"{ctx.prefix}{ctx.command.name} ",1)
    if len(tmp) > 1:
        return tmp[1].split(' ')
    else:
        await ctx.send("T'as pas oublié de mettre une couleur p'tit zigoto ? 😝")
        raise Exception

async def getEffectFromParams(ctx):
    tmp = ctx.message.content.split(f"{ctx.prefix}{ctx.command.name} ",1)
    if len(tmp)>1:
        effect = tmp[1].split(' ')
    else:
        await ctx.send("T'as pas oublié de mettre un effet p'tit zigoto ? 😝")
        print("no effect param")
        raise Exception
    
    if len(effect) > 1:
        await ctx.send("On ne peut saisir qu'un seul effet par commande 🙁")
        print("too many effects params")
        raise Exception
    
    return tmp[1].split(' ')

async def getRGBcodesFromParamsArray(input_txt_name_colors, ctx):
    RGB_codes  = []
    for input_color in input_txt_name_colors:
        try:
            RGB_codes.append(config["light"]["colors"][input_color])
            print("color found")
        except Exception as e:
            print("color name is not in colors list")
            await ctx.send("\""+input_color+"\" n'est pas valide 🙁")
            raise e
    if len(RGB_codes) < 3:
        for i in range(len(RGB_codes),3):
            RGB_codes.append({"R" : 0,"G" : 0,"B" : 0})
    return RGB_codes

async def getEffectIDfromName(input_txt_name_effect, ctx):
    output_effect_ID = True
    try:
        output_effect_ID = config["light"]["effects"].index(input_txt_name_effect)
        print("effect found")
    except Exception as e:
        print("effect name is not in effects list")
        await ctx.send("\""+input_txt_name_effect+"\" n'est pas valide 🙁")
        raise e
    return output_effect_ID

def saveCurrentState():
    r = httpx.get(config["url"])
    if handle_wled_return(r) == True:
        global savedState
        savedState = json.loads(r.text)
    return handle_wled_return(r)

def BUILDER_set_colors(frame,RGB_codes):
    for RGB_color_item in RGB_codes:
        frame["seg"][0]["col"].append([RGB_color_item["R"],RGB_color_item["G"],RGB_color_item["B"],0])
    return frame

def BUILDER_set_colors_root(RGB_codes):
    data={"seg":[{"col":[]}]}
    return BUILDER_set_colors(data,RGB_codes)

def BUILDER_set_effect(frame,effect_ID):
    frame["seg"][0]["fx"] = effect_ID
    return frame

def BUILDER_append_effect_root(effect_ID):
    data={"seg":[{"col":[]}]}
    return BUILDER_set_effect(data,effect_ID)

def BUILDER_send_raw_frame(frame):
    JSONized=json.dumps(frame)
    r = httpx.post(config["url"], data=JSONized)
    print(r.text)
    return handle_wled_return(r)

def BUILDER_set_fx_speed(frame, fx_speed):
    frame["seg"][0]["sx"] = fx_speed
    return frame

def BUILDER_set_fx_intensity(frame, fx_intensity):
    frame["seg"][0]["ix"] = fx_intensity
    return frame

def BUILDER_set_raw_value(frame, key, value):
    frame[key] = value
    return frame


class Bot(commands.Bot):

    def __init__(self):
        # Initialise our Bot with our access token, prefix and a list of channels to join on boot...
        super().__init__(token=credentials["token"], prefix='!', initial_channels=['maskim64'])

    async def event_ready(self):
        # We are logged in and ready to chat and use commands...
        print(f'Logged in as | {self.nick}')
        print(f'User id is | {self.user_id}')

    @commands.command()
    async def hello(self, ctx: commands.Context):
        # Send a hello back!
        await ctx.send(f'Hello {ctx.author.name}!')
        print('Hello received')
    
    @commands.command()
    async def color(self, ctx: commands.Context):
        print('"?color" received')
        effect = "solid"
        try:
            input_txt_name_colors = await getColorsFromParams(ctx)
            RGB_codes             = await getRGBcodesFromParamsArray(input_txt_name_colors, ctx)
            effect_ID             = await getEffectIDfromName(effect, ctx)
        except Exception as e:
            return
        frame = BUILDER_set_colors_root(RGB_codes)
        frame = BUILDER_set_effect(frame,effect_ID)
        BUILDER_send_raw_frame(frame)
        for color in input_txt_name_colors:
            count["colors"][color]+=1
        updateCountsFile(count)

    @commands.command()
    async def effect(self, ctx: commands.Context):
        print('"?effect" received')
        try:
            input_txt_name_effect = await getEffectFromParams(ctx)
            effect_ID = config["light"]["effects"].index(input_txt_name_effect[0])
            print("effect found")
        except Exception:
            print("effect not found")
            return
        frame = BUILDER_append_effect_root(effect_ID)
        BUILDER_send_raw_frame(frame)
        for effect in input_txt_name_effect:
            count["effects"][effect]+=1
        updateCountsFile(count)

    @commands.command()
    async def police(self, ctx: commands.Context):
        print('"!police" received')
        input_txt_name_colors = ["red","blue"]
        effect = "blink"
        try:
            RGB_codes = await getRGBcodesFromParamsArray(input_txt_name_colors, ctx)
            effect_ID = await getEffectIDfromName(effect, ctx)
        except Exception as e:
            return
        saveCurrentState()
        frame = BUILDER_set_colors_root(RGB_codes)
        frame = BUILDER_set_effect(frame,effect_ID)
        frame = BUILDER_set_fx_speed(frame,230)
        frame = BUILDER_set_fx_intensity(frame, 127)
        frame = BUILDER_set_raw_value(frame, "transition", 0)
        BUILDER_send_raw_frame(frame)
        time.sleep(5)
        BUILDER_send_raw_frame(savedState)

    @commands.command()
    async def rgb(self, ctx: commands.Context):
        print('"!rgb" received')
        input_txt_name_colors = ["red","blue","lime"]
        effect = "blink"
        try:
            RGB_codes = await getRGBcodesFromParamsArray(input_txt_name_colors, ctx)
            effect_ID = await getEffectIDfromName(effect, ctx)
        except Exception as e:
            return
        saveCurrentState()
        frame = BUILDER_set_colors_root(RGB_codes)
        frame = BUILDER_set_effect(frame,effect_ID)
        frame = BUILDER_set_fx_speed(frame,230)
        BUILDER_send_raw_frame(frame)
        time.sleep(5) # Sleep for 3 seconds
        BUILDER_send_raw_frame(savedState)

    # @commands.command()
    # async def flash(self, ctx: commands.Context):
    #     print('"!flash" received')
    #     input_txt_name_colors = ["red","black","black"]
    #     effect = "blink"
    #     try:
    #         RGB_codes = await getRGBcodesFromParamsArray(input_txt_name_colors, ctx)
    #         effect_ID = await getEffectIDfromName(effect, ctx)
    #     except Exception:
    #         return
    #     saveCurrentState()
    #     frame = BUILDER_set_colors_root(RGB_codes)
    #     frame = BUILDER_append_effect(frame,effect_ID)
    #     frame = BUILDER_set_fx_speed(frame,230)
    #     BUILDER_send_raw_frame(frame)
    #     time.sleep(5) # Sleep for 3 seconds
    #     BUILDER_send_raw_frame(savedState)


bot = Bot()
bot.run()